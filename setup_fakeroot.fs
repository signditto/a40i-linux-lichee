#!/bin/sh
set -e
PWD=`pwd`
chown -h -R 0:0 ${PWD}/out/sun8iw11p1/linux/common/buildroot/target
${PWD}/tools/pack/common/buildroot/host/usr/bin/makedevs -d ${PWD}/tools/pack/common/devices/_device_table.txt ${PWD}/out/sun8iw11p1/linux/common/buildroot/target


PATH="${PWD}/tools/pack/common/buildroot/host/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games" mke2img -d ${PWD}/out/sun8iw11p1/linux/common/buildroot/target -G 4 -R 1 -B 1389999 -I 0 -o ${PWD}/out/sun8iw11p1/linux/common/buildroot/images/rootfs.ext2
#${PWD}/tools/build/bin/make_ext4fs -l 1366M  ${PWD}/out/sun8iw11p1/linux/common/rootfs.ext4 ${PWD}/out/sun8iw11p1/linux/common/buildroot/target


ln -sf ${PWD}/out/sun8iw11p1/linux/common/buildroot/images/rootfs.ext2 ${PWD}/out/sun8iw11p1/linux/common/buildroot/images/rootfs.ext4

cp ${PWD}/out/sun8iw11p1/linux/common/buildroot/images/rootfs.ext2 ${PWD}/out/sun8iw11p1/linux/common/rootfs.ext4
