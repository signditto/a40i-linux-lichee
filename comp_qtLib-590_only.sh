#!/bin/bash
echo "ui_autobuild:build.sh"

cd  buildroot-201611/dl
if [ -d qt-everywhere-opensource-src-5.9.0 ];then
( 
    echo "qt-everywhere-opensource-src-5.9.0 have been compiled before,skip!!!"
    cd ../../
    ./build.sh
    ./build.sh pack
)
else(
    echo "ui_autobuild:tar qt-everywhere-opensource-src-5.9.0.tgz"
    tar  -zxvf  qt-everywhere-opensource-src-5.9.0.tgz

    echo "ui_autobuild:cp setenvs.sh"
    cp  -f  setenvs.sh  qt-everywhere-opensource-src-5.9.0/

    echo "ui_autobuild:source  setenvs.sh"
    cd qt-everywhere-opensource-src-5.9.0/

    source  ./setenvs.sh

    echo "ui_autobuild:makeconfig"
    makeconfig

    echo "ui_autobuild:makeall"
    makeall

    echo "ui_autobuild:makeinstall"
    makeinstall

    cd ../../../
    ./build.sh
    echo "ui_autobuild: pack"
    ./build.sh pack
)
fi
